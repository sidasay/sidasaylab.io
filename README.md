# GitLab Pages

This is the project that hosts <https://pages.gitlab.io> and is built by
GitLab Pages.

## Issue tracker

Use the issue tracker to report problems or propose enhancements to the website,
and provide feedback to the various examples that are hosted under
<https://gitlab.com/groups/pages>. This is also the place to request an example
website that currently doesn't exist.

Issues related to `https://pages.gitlab.io` are labeled as ~Website, whereas
example requests are labeled as ~"Example proposal".

## Building locally

This website is built with [Middleman]. To start hacking, follow the steps
below:

1. [Install Ruby][ruby]
1. Install bundler: `sudo gem install bundler`
1. Clone this project: `git clone https://gitlab.com/pages/pages.gitlab.io.git`
1. Change your directory: `cd pages.gitlab.io`
1. Install middleman and its dependencies: `bundle install`
1. Run middleman: `bundle exec middleman`
1. Visit the site at http://localhost:4567

The website will be automatically reloaded with any changes you make.

## Contributing

You are welcome to contribute to this project or to the examples hosted under
the pages group.

See [CONTRIBUTING.md](CONTRIBUTING.md) for more information.

## License

The contents of the website's source code (HTML, CSS, Javascript) are licensed
under MIT, and the text under [CC BY-SA 4.0][cc].

[middleman]: https://middlemanapp.com
[ruby]: https://www.ruby-lang.org/en/downloads/
[cc]: https://creativecommons.org/licenses/by-sa/4.0/
