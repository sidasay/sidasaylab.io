isElementOnScreen = ($el, scrollTop) ->
  # Get very bottom of element
  elementBottom = $el.offset().top + $el.outerHeight()
  # Get very top of element
  elementTop = $el.offset().top - scrollTop
  if elementTop <= $(window).height() and elementBottom - scrollTop >= 0
    # Element is on screen
    true
  else
    # Element is not screen
    false

$ ->
  $('.js-scroll-to').on 'click', (e) ->
    e.preventDefault()

    $target = $ $(@).attr('href')
    $('body').animate
      scrollTop: $target.offset().top

  # Scroll effect on steps
  $steps = $ '.js-step, .js-learn-more'
  $(window).on 'scroll', ->
    $steps.each ->
      isOnScreen = isElementOnScreen $(@), ($(window).scrollTop() - 150)

      if isOnScreen && !$(@).hasClass 'is-visible'
        $(@).addClass 'is-visible'
