<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Adding a new site example](#adding-a-new-site-example)
- [README template](#readme-template)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Adding a new site example

The steps to add a new static site example are:

1. Create a new project named after the static generator (lowercase)
1. Add a project description which must follow the schema:

    > Example `<Name>` site using GitLab Pages: https://pages.gitlab.io/`<name>`

    where `<Name>` is the name of the static generator and the URL's `<name>`
    is taken after the project's name.

1. Add a `README.md` that has specific information, according to
   the [README template](#readme-template)

1. Add a new issue to https://gitlab.com/pages/pages.gitlab.io/issues, containing:
    - SSG name / website
    - Your working example: project url / website url
    - Yours (optional) and the SSG's Twitter handle (we'll thank you for contributing!) 

## README template

The `README.md` file must contain the following information in this order:

1. Link to the build status badge
1. Introduction: what this project is about, provide links to:
    1. Official project main page
    1. <https://pages.gitlab.io>
    1. Pages documentation  <http://doc.gitlab.com/ee/pages/README.html>
1. Building locally
    1. Install requirements
    1. Run local server if any
1. GitLab CI
1. Difference between user/project Pages
    - How to convert to user Pages
1. Did you fork this project?
    - Remove fork relationship

---

Use the following text as a base and build upon it:

```
![Build Status](https://gitlab.com/pages/<project>/badges/master/build.svg)

---

Example [<Project>] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation http://doc.gitlab.com/ee/pages/README.html.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

contents of .gitlab-ci.yml in codeblock

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] <project>
1. Generate the website: `make html`
1. Preview your project: `make serve`
1. Add content

Read more at <Project>'s [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[<project>]: http://link-to-project-main-page
[install]: http://link-to-install-page
[documentation]: http://link-to-main-documentation-page
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages

----

Forked from @<username>
```

---

The changes you'll have to make are:

1. Replace `<project>` and `<Project>` with the project name (**tip:** use your
   editor's search and replace function)
1. Explain the contents of `.gitlab-ci.yml`
1. Add some brief steps how to build and hack locally
1. Replace the _links_ at the bottom of the README to:
  1. `[<project>]`: The project's homepage
  1. `[install]`: The project's installation guide
  1. `[documentation]`: The project's documentation website
  1. `<username>`: Your username at GitLab 
1. Use [doctoc][] to create or update the Table Of Contents: `doctoc --gitlab README.md`
   Make sure that the badge and the project info are on top. See the README
   template to take an example.

Leave any other information as-is.

[doctoc]: https://github.com/thlorenz/doctoc/
